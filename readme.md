# Framework NodeJS

## Caracteristicas principales
 * carga dinamica de modulos de la aplicacion
 * carga de recursos de configuración de la aplicación
 * carga de modulos en orden definido
 * deinicion de variables de la aplicacion segmentadas por modulo y core del framework
 * Adicion por defecto de libreria para realizar trazabalidad de la aplicacion con logging
 
## Tecnologias utilizadas

* log4js
* NodeJS
* Redis
* ElasticSearch
* SqlServer
* Excepciones Personalizadas

Tipos de valores para la configuracion de carga de porpiedades de los modules
* inherit = Hereda la configuraciones por defecto del modulo
* merge = Hereda las configuracion por defecto del modulo pero permite el remplazo de parametros desde el achivo de configuracion seleccionado desde el proceso de carga de archivos de configuracion del framewrk
* replace = remplaza todas las propiedades del archivo de conifguracion por defecto (se debe tener cuidado con esta opcion ya que se debe tener certeza que se estan seteando todos los valores de configuracion de cada modulo)


## Manejo de errores excepciones
``
{
  "1000":"Error en la capa del controlador",
  "2000":"Error en la capa de servicio",
  "3000":"Error en la capa de Repository"
}
``
 