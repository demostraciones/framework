"use strict";


/**
 * Importacion de dependencias
 */
const Log4JsService = require("./service/Log4JsService");

/**
 * @author Brayan Hamer Rodriguez Sanchez -  bhamerrsanchez@gmail.com
 * @copyright SmartSoft -2017
 */
class Boot{
    /**
     * Constructor de la clase
     */
    constructor() {
        global.Logging = require('log4js');
        this._log4JsService = new Log4JsService();
    }

    /**
     * Función de configuracion del modulo
     */
    setup() {
        console.log("Iniciando configuracion de log4s");
        this._log4JsService.setup();
    }

    reload(config){
        this._log4JsService.reload(config);
    }
}
module.exports = Boot;
