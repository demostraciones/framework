"use strict";

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class RouterController extends global.app.class.core.Controller {

    /**
     * Constructor de la clase
     * @param {string} filename ruta del archivo que hereda esta clase
     */
    constructor(filename, pathRoute) {
        super(filename);
        this._pathRoute = pathRoute;
        this._route = global.router;
    }


}

module.exports = RouterController;