"use strict";

/**
 *Sección de importación de dependencias
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));
const path = require('path');
const express = require('express');
const Router = express.Router();
const bodyParser = require('body-parser');
const http = require('http');
const RouterController = require('./bo/RouterController');

/**
 * Clase de carga de modulos de express y configuraciones de express
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSfot - 2017
 */

class Boot extends global.app.class.core.ModuleBootCore {

    constructor() {
        super(__filename);
        this._config = global.app.configuration.core.modules.express.config;
        global.router = Router;
        global.app.class.modules[this.module].RouterController = RouterController;
        global.appExpress = express();
        global.httpServer = http.Server(global.appExpress);
    }

    setup() {
        log.info("Configurando express");
        global.appExpress.use(bodyParser.urlencoded({extended: false}));
        global.appExpress.use(bodyParser.json());
        global.appExpress.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header("Access-Control-Allow-Headers", 'Content-Type, Authorization, Content-Length, X-Requested-With');
            if (req.method === 'OPTIONS') {
                res.send(200);
            } else {
                next();
            }
        });
        global.appExpress.use(global.Logging.connectLogger(log, {
            level: 'auto',
            format: ':method :url',
            nolog: '\\.gif|\\.jpg$'
        }));

        global.appExpress.use(this._config.prefix_rest, global.router);
        global.httpServer.listen(this._config.port, () => {
            log.info('aplicacion web express escuchando por el puerto : ' + this._config.port);
            log.info(`url : http://localhost:${this._config.port}${this._config.prefix_rest}`);
        });
        this._configStaticContent();
        this.loadRoutes();

    }
    _configStaticContent(){
        if(this._config.static.enable){
            log.info(`realizando configuracion de contenido estatico`);
            this._config.static.config.forEach((value) => {
                let contentPath = '';
                let next = false;
                if (value.type==='relative'){
                    contentPath = value.path;
                    next = true;
                }else if (absolute){
                    contentPath = path.resolve(this.CoreService.getGlobalCoreVariable('PATH_PROJECT_ROOT'),value.path);
                    next = true;
                }else {
                    log.error('valor de type del contenido debe ser definido como (realtive, absolute)');

                }
                if (next){
                    if(value.publicPath || value.publicPath==='/'){
                        global.appExpress.use(express.static(contentPath));
                    }else {
                        global.appExpress.use(value.publicPath,express.static(contentPath));
                    }
                }
            });

        }
        else {
            log.info(`la pocpion de contenido estatico esta sin habilitada`);
        }
    }

    loadRoutes() {
        try {
            global.router.get('/', (req, res) => {
                res.json({status: 200});
            });
            global.router.get('/status', (req, res) => {
                res.json({status: 200});
            });
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports = Boot;