"use strict";

/**
 *@author Andres Felipe Gonzalez
 * clase que gestiona los errores en las consultas o conexion a redis
 *@copyright SmartSoft 2017
 */

class RedisException extends global.app.class.exception.Exception {
    /**
     * Constructor de la clase
     * @param filename ruta del origen de la exepcion
     * @param object datos informativos de la exepcion
     * @param status
     */
    constructor(filename, object, status) {
        super('2002', filename, object, status);
    }
}

module.exports =  RedisException;
