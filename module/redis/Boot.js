"use strict";
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * clase que configura e inicial el modulo de redis en el framework
 * @copyright SmartSoft - 2017
 */
const RedisService = require("./service/RedisService");
class Boot extends global.app.class.core.ModuleBootCore {
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
        global.app.instances.redisService = new RedisService();
    }
    setup() {
        log.info("Cargando modulo de RedisService")
    }
}

module.exports = Boot;
