"use strict";
/**
 * Sección de definición de las dependencias
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));
const RedisException = require('../exception/RedisException');
const Redis = require('redis');
/**
 * Clase de manejo de conexiones con redis
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */
let instance = null;
class RedisService extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     * @param config objeto json con los parametros de configuracion de la conexion con
     * el servidor de Redis
     */
    constructor() {
        super(__filename);
        log.trace("Configuración de conexion a redis");

        if (!instance) {
            instance = this;
            this._config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
            this._connection = null;
        }
        return instance;
    }

    /**
     * Función que obtiene la conexion con redis
     * @param db base de datos con la que se va conectar
     * @return
     */
    getConnection(db) {
        this._config.db = typeof db ==="undefined" ? this._config.db:db;
        return Redis.createClient(this._config);
    }

    /**
     * Función que permite obtener los valores de los keys de redis de acuerdo al patrón recibido
     * @param pattern String Patrón que se va a utilizar para la búsqueda en redis
     * @param {Int} db base de datos seleccionada, si no se envia el numero por defecto va en el parametro espeficicado en el json
     * @return Promise Promesa con la respuesta de la petición
    **/
    getKeyValues(pattern,db){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection(db);
            try {
                redis.keys(pattern, (error, keys) => {
                    redis.mget(keys, (error, values) => {
                        redis.quit();
                        resolve(values);
                    });
                });
            } catch(error) {
                reject(error);
            }
        });
    }

    /**
     * Función que sirve para agregar / actualizar índices en Redis
     * El formato establecido para los índices de reglas es el siguiente: 'sbuId:listId:element' '{ListElement}'
     * Ejemplo: '001:1:25112' '{"sbuId": "001", "listId": 1, "value": "25112", "expirationDate": 0}'
     * El primer bloque corresponde a los datos de la lista ('sbuId:listId:element')
     * El segundo bloque corresponde con un objeto de tipo ListElement con sus respectivos valores
     * ('{"sbuId": "001", "listId": 1, "value": "25112", "expirationDate": 0}')
     * @param key String Nombre con el cual se va a identificar el índice
     * @param value String Valor que va a ser asignado al índice
     * @param {Int} db base de datos seleccionada, si no se envia el numero por defecto va en el parametro espeficicado en el json
     * @param expireSeconds Number Fecha de expiración del índice expresado en segundos
     * @param expireMilliseconds Number fecha de expiración del índice expresado en milisegundos
     * @param setKey Boolean Parámetro que indica si se crea o no el índice en el caso que exista en Redis
     * @return Promise Retorna una promesa con la información del proceso realizado
     */

    setValue(key, value,db,expireSeconds = null, expireMilliseconds = null, setKey = null){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection(db);
            try {
                redis.set(key, value, (error, result) => {
                    if(error){
                        reject(new RedisException(__filename,{error: error.message}));
                    } else {
                        if(expireSeconds){
                            redis.expire(key, expireSeconds);
                        } else if(expireMilliseconds){
                            redis.expire(key, expireMilliseconds);
                        }
                        redis.quit();
                        resolve(result);
                    }
                });
            } catch (error){
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Función que sirve para agregar / actualizar múltiples índices en Redis
     * Recibe como parámetro un Array de objetos con la siguiente estructura:
     * [{'key' : '001:1:25112' , 'value':'{"sbuId": "001", "listId": 1, "value": "25112", "expirationDate": 0}', 'expires' : '0'}]
     * key: Índice conformado por sbuId:listId:listElementValue, value: Valor datos del elemento, expires: Tiempo de expiración en segundos
     * @param values Array Arreglo que contiene objetos con los índices que se van a crear ó actualizar en Redis
     * @param db Base de datos donse se guardaran los valores
     * @return Promise Retorna una promesa con la información del proceso realizado
     */

    setValues(values,db){
        return new Promise((resolve, reject) => {
            let temp = values;
            let redis = this.getConnection(db);
            try {
                values.forEach(element => {
                    if(parseInt(element.expires) === 0){
                        redis.set(element.key, element.value, (error, result) => {
                            if(error){
                                log.error(values);
                                throw new RedisException(__filename,{error: error.message});
                            }
                            else{
                                redis.quit();
                                resolve('ok');
                            }
                        });
                    } else {
                        if(element.expires > 0){
                            redis.set(element.key, element.value, (error, result) => {
                                if(error){
                                    throw new RedisException(__filename,{error: error.message});
                                } else {
                                    redis.expire(element.key, element.expires);
                                    redis.quit();
                                    resolve('ok');
                                }
                            });
                        }
                    }
                });
            } catch (error){
                redis.quit();
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Función que sirve para obtener los índices registrados en Redis
     * @param pattern String Cadena de texto que contiene el patrón de búsqueda en Redis
     * @return Promise Retorna una promesa con la información del proceso realizado
     */
    getKeys(pattern){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection();
            try {
                redis.keys(pattern, (error, result) => {
                    redis.quit();
                    if(error){
                        reject(new RedisException(__filename,{error: error.message}));
                    }
                    resolve(result);
                });
            } catch(error) {
                redis.quit();
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Función que sirve para obtener los valores de los índices en Redis
     * @param keys String Índice o arreglo que va a ser consultado en Redis
     * @param {Int} db base de datos seleccionada, si no se envia el numero por defecto va en el parametro espeficicado en el json
     * @return Promise Retorna una promesa con la información del proceso realizado
     */
    getValues(keys,db){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection(db);
            try {
                redis.mget(keys, (error, result) => {
                    redis.quit();
                    if(error){
                        reject(new RedisException(__filename,{error: error.message}));
                    } else {
                        resolve(result);
                    }
                });
            } catch(error) {
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    getListValues(pattern){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection();
            try {
                redis.keys(pattern, (error, keys) => {
                    if(error){
                        redis.quit();
                        reject(new RedisException(__filename,{error: error.message}));
                    } else {
                        redis.mget(keys, (error, values) => {
                            redis.quit();
                            if(error){
                                reject(new RedisException(__filename,{error: error.message}));
                            } else {
                                let listValues = [];
                                values.forEach( value => {
                                    listValues.push(JSON.parse(value))
                                });
                                resolve(listValues);
                            }
                        });
                    }
                });
            } catch (error){
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Función que sirve para eliminar índices en Redis
     * @param keys String Índice o arreglo que va a ser eliminado en Redis
     * @return
     */
    deleteValue(keys){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection();
            try {
                redis.del(keys, (error, result) => {
                    if(error){
                        reject(new RedisException(__filename,{error: error.message}));
                    } else {
                        resolve(result);
                    }
                });
            } catch(error) {
                redis.quit();
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Obtiene la cantidad de llaves en una base de datos
     * @param db base de datos a consultar
     * @returns {Promise<any>}
     */
    getSizeBD(db){
        return new Promise((resolve, reject) => {
            let redis = this.getConnection(db);
            try {
                redis.dbsize((error, result) => {
                    if(error){
                        reject(new RedisException(__filename,{error: error.message}));
                    } else {
                        resolve(result);
                    }
                });
            } catch(error) {
                redis.quit();
                reject(new RedisException(__filename,{error: error.message}));
            }
        });
    }

    /**
     * Funcion que elimina todas las llaves de la base de datos de redis
     * @param db base de datos a limpiar
     */
    flush(db){
        return new Promise ((resolve,reject)=>{
            let redis = this.getConnection(db);
            redis.flushdb((err, succeeded)=>{
                if(err){
                    reject(err);
                }
                else{
                    resolve(succeeded);
                }

            });
        })
    }

    /**
     * Permite cargar arrays a redis siguendo la siguiente estructura
     * "key", ["test keys 1", "test val 1", "test keys 2", "test val 2"]
     * @param indexName - "key" o indice de la matriz que se almacena en redis
     * @param values - array de valores
     * @returns {Promise<any>} respuesta de redis
     */
    redisBulk(indexName,values){
        return new Promise((resolve,reject)=>{
            let redis = this.getConnection();
            redis.hmset(indexName,values,(err,result)=>{
                if(err){
                    reject(err);
                }
                else{
                    resolve(result);
                }
            });
        });
    }
}
module.exports = RedisService;