/**
 * Utilidades del sistema
 * @author Oscar Beltran
 * @copyright Smartsoft - 2017
 */
"use strict";

let Validator = require('jsonschema').Validator;
const {useCustomDate} = require('../../../../resources/application');

class UtilService {
    constructor() {
        this.validator = new Validator();
    }

    validateSchema(object, schema) {
        return this.validator.validate(object, schema);
    }

    /**
     * Función Utilizada para la generación de logs de prueba de integracion con logstash
     */
    exampleLogGenerator(log) {
        let high = 10000;
        let low = 0;
        let time = Math.random() * (high - low) + low;
        log.info("Tiempo de ejecucion [%d]", time);
        setTimeout(() => {
            let high = 12;
            let low = 0;
            let number = Math.random() * (high - low) + low;
            switch (Math.floor(number)) {
                case 1: {
                    log.info("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                case 2: {
                    log.error("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                case 3: {
                    log.warn("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                case 4: {
                    log.trace("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                case 5: {
                    log.fatal("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                case 6: {
                    log.debug("Inicio completo de la aplciación : [" + number + "]");
                    break;
                }
                default: {
                    log.debug("Default : [" + number + "]");
                    break;
                }
            }
            this.exampleLogGenerator(log);
        }, time);
    }

    /**
     * Función para validar si un campo se encuentra vacío
     */

    static isEmpty(value) {
        return (value === '' || value === null || value === undefined);
    }

    /**
     * @author Andres Felipe Gonzalez
     * Funcion que valida si se usa la fecha actual o una fecha custom en las consultas sql
     * */
    static getDate(SBUId, mainSourceId) {
        return global.elasticService.get('online_spots', `${SBUId}_${mainSourceId}`)
            .then(response => {
                let lastTransaction = JSON.parse(response.last);
                return lastTransaction.Fecha_Hora_Transaccion;
            })
            .catch(() => {
                const {value, date} = useCustomDate;
                if (value) {
                    return date;
                } else {
                    return 'Getdate()';
                }
            });
    }

    /**
     * Función que reemplaza los atributos del objeto en el string recibido
     * @param {string} message
     * @param {object} object
     * @returns {*|string|void|XML}
     * @private
     */
    interpolate(message, object) {
        return message.replace(/{([^{}]*)}/g, (a, b) => {
                let r = object[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    }

    /**
     * Obtiene el nombre del campo por medio del nombre real de la tabla
     * @param {String} fieldAlias
     */
    static getFieldNameByAlias(tableAlias, fieldAlias) {
        let fieldName = null;
        try {
            tableAlias = this.getALiasTableByName(tableAlias);
            if (fieldAlias in global.tablesInfo[tableAlias].TableFields) {
                fieldName = global.tablesInfo[tableAlias].TableFields[fieldAlias].name;
            }
            else {
                let fieldNameExtracted = Object.keys(global.tablesInfo[tableAlias].TableFields).filter(alias => {
                    return global.tablesInfo[tableAlias].TableFields[alias].name == fieldAlias;
                });

                fieldName = fieldNameExtracted[0];
            }
        }
        catch (err) {
            err.method = "getFieldNameByAlias";
            err.table = tableAlias;
            err.alias = fieldAlias;
            throw new AliasNameException(__filename, err);
        }

        return fieldName;
    }

    /**
     * Retorna el alias de una tabla basado en su nombre real
     * @param {String} tableName
     */
    static getALiasTableByName(tableName) {
        let aliasName = Object.keys(global.tablesInfo).filter(alias => {
            return global.tablesInfo[alias].Name == tableName;
        });


        return (aliasName.length) ? aliasName[0] : tableName;
    }

}

module.exports = UtilService;