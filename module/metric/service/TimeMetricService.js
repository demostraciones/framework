"use strict";

const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Utilidad para medir tiempos de ejecución
 * @author Oscar Beltran
 * @copyright SmartSoft 2017
 */
class TimeMetricService {

    constructor(){
        this.times = {};
        this.itCount = 0;
    }

    /**
     * Inicializa el temporizador
     * @param {string} label
     * @param {number} iteration
     */
    init(label){
        this.times[this.itCount] = this.times[this.itCount] || {};
        if(typeof this.times[this.itCount][label] === "undefined"){
            this.times[this.itCount][label] = { "start" : Date.now() };
        }
        else{
            log.error(`La etiqueta ${label} en la iteración ${this.itCount} está siendo utilizada`);
        }
    }

    /**
     * Detiene el temporizador y registra el resultado
     * @param {string} label
     * @param {number} iteration
     */
    stop(label){
        let time = (this.times[this.itCount] && this.times[this.itCount][label] && this.times[this.itCount][label].start) ? this.times[this.itCount][label] : null;
        if(time !== null){
            this.times[this.itCount][label].end = Date.now();
            this.times[this.itCount][label].time = this.times[this.itCount][label].end - this.times[this.itCount][label].start;
        }
    }

    /**
     * retorna el resultado de la iteración actual
     * @param iteration
     */
    getCurrentResult(){
        return this.times[this.itCount];
    }

    /**
     * Retorna el historico de resultados
     * @returns {{}|*}
     */
    getResults(){
        return this.times;
    }

    /**
     * Incrementa el contador de iteraciones
     */
    newIteration(){
        this.itCount++;
    }

    /**
     * Retorna instancia
     * @returns {null|StopwatchService}
     */
    static getInstance(name){
        this.instances = this.instances || {};
        if(!this.instances[name]){
            this.instances[name] = new TimeMetricService();
        }
        return this.instances[name];
    }
}

module.exports = TimeMetricService;