"use strict";

/**
 *@author Julian Borray
 * Clase que gestiona los errors al hacer consultas en mongodb
 *@copyright SmartSoft 2017
 */

class MongoException extends global.app.class.exception.Exception {
    /**
     * Constructor de la clase
     * @param filename ruta del origen de la exepcion
     * @param object datos informativos de la exepcion
     * @param status
     */
    constructor(filename, object, status) {
        super('2003', filename, object, status);
    }
}

module.exports =  MongoException;
