"use strict";
/**
 * Sección de definición de las dependencias
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));
const MongoException = require('../exception/MongoException');
const MongoClient = require('mongodb').MongoClient;
/**
 * Clase de manejo de conexiones con redis
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */
let instance = null;
class MongoService extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     * @param config objeto json con los parametros de configuracion de la conexion con
     * el servidor de mongodb
     */
    constructor() {
        super(__filename);
        log.debug("Configuración de conexion a mongo");

        if (!instance) {
            instance = this;
            this._config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
        }
        return instance;
    }

    /**
     * Función que obtiene la conexion con MongoDB
     * @return cliente con la conexion
     */
    getConnection() {
        return new Promise((resolve,reject)=>{
            try{
                MongoClient.connect(this._config.host+":"+this._config.port, function(err, client) {
                    if(err){
                        reject(err);
                    }else{
                        resolve(client);
                    }
                });
            }catch(err){
                reject(new MongoException(__filename,err,500));
            }
        });
    }

    /**
     * Funcion para insertar arrays de datos en mongo
     * @param {String} collection Nombre de la coleccion donde se van a insertar los documentos
     * @param {Array} data documentos en formato [{},{}]
     */
    insertValues(collection,data){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);
                    selectedCollection.insertMany(data,(err,result)=>{
                        if(err){
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }else{
                            client.close();
                            resolve(result);
                        }
                    });
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }


    /**
     * Funcion para insertar un valor unico en mongo
     * @param collection
     * @param JsonValue valor en formato json {}
     */
    insertValue(collection,JsonValue){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);
                    let object =[];
                    object.push(JsonValue);
                    selectedCollection.insertOne(object,(err,result)=>{
                        if(err){
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }else{
                            client.close();
                            resolve(result);
                        }
                    });
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Elimina todos los documentos de una coleccion
     * @param collection nombre de la coleccion
     */
    cleanCollection(collection){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);
                    selectedCollection.deleteMany({},(err,result)=>{
                        if(err){
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }
                        else{
                            client.close();
                            resolve(result)
                        }
                    })
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Obtiene el tamaño de una coleccion, la cantidad de documentos
     * @param collection
     */
    getSizeBD(collection){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);
                    selectedCollection.count().then((size)=>{
                        client.close();
                        resolve(size);
                    },(err)=>{
                        client.close();
                        reject(new MongoException(__filename,{error: err.message},500));
                    })
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Busca y retorna los valores segun el parametro pasado
     * @param collection nombre de la coleccion donde buscara los valores
     * @param pattern ej= {"key":"llave"}
     */
    findValues(collection,pattern){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);

                    selectedCollection.find(pattern).toArray((err,result)=>{
                        if(err){
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }
                        else{
                            client.close();
                            resolve(result);
                        }
                    })
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Reemplaza o actualiza un valor
     * @param collection coleccion donde buscara
     * @param filter filtro para indicar que valor debe reemplazar/actualizar
     * @param values valores que reemplazaran los resultdos del filtro
     */
    replaceValues(collection,filter,values){
        return new Promise((resolve,reject)=>{
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);

                    selectedCollection.replaceOne(filter,values,(err,result)=> {
                        if (err) {
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }
                        else {
                            client.close();
                            resolve(result);
                        }
                    },(err)=>{
                        reject(new MongoException(__filename,{error: err.message},500));
                    })
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Obtiene un valor de la coleccion
     * @param collection
     */
    getFirstValue(collection){
        return new Promise((resolve, reject) => {
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);

                    selectedCollection.find({}).limit(1).toArray((err,result)=>{
                        if(err){
                            client.close();
                            reject(new MongoException(__filename,{error: err.message},500));
                        }
                        else{
                            client.close();
                            resolve(result);
                        }
                    })
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }

    /**
     * Elimina un valor de la coleccin
     * @param collection
     * @param pattern
     */
    removeValue(collection,pattern){
        return new Promise((resolve, reject) => {
            try{
                this.getConnection().then((client)=>{
                    let db = client.db(this._config.db);
                    let selectedCollection = db.collection(collection);

                    selectedCollection.remove(pattern,(err,result)=>{
                        if(err){
                            log.error(err);
                            reject(new MongoException(__filename,{error: err.message},500));
                        }
                        else{
                            resolve(result);
                        }
                    });
                },(err)=>{
                    reject(new MongoException(__filename,{error: err.message},500));
                })
            }
            catch(err){
                reject(new MongoException(__filename,{error: err.message},500));
            }
        });
    }
}
module.exports = MongoService;