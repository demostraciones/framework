"use strict";
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
const MongoService = require("./service/MongoService");
class Boot extends global.app.class.core.ModuleBootCore {
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
        global.app.instances.mongoService = new MongoService();
    }
    setup() {
        log.info("Cargando modulo de mongodb")
    }
}

module.exports = Boot;
