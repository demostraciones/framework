"use strict";

/**
 * Clase que representa la estructura del logger, permitiendo realizar la
 * carga de las configruaciones realizadas en el archivo application.json
 * y en caso de no encontrar esta configuracion, realiza la carga de las
 * configuraciones por defecto del archivo config.json del modulo
 * log4js
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */
 class Log4JsService {
     constructor(config) {
         if (config) {
             this.config = config;
         } else {
             this.config = require('../resources/application.json');
         }
     }

     /**
      * Función que realiza la configuración del logger
      */
     setup() {
         global.Logging.getPackageFileName = this.getPackageFileName;

         let tokens = {
             "ln": function () {
                 var error = new Error().stack.split("\n");
                 var message = error[error.length - 1].replace(')', '').split(':');
                 return message[message.length - 2] + ':' + message[message.length - 1];
             }
         };
         this.config['appenders'].forEach(function (value, key) {
             if (value.hasOwnProperty('layout')) {
                 value['layout']['tokens'] = tokens;
             }
         });

         global.Logging.configure(this.config);
     }

     reload(config){
         this.config = config;
         this.setup();
     }

     /**
      * Funcion que realiza la configuracion inicial de sistema
      * de logging basado en el archivo de configuracion de la
      * aplicacion, se utiliza log4js para manejar el proceso de
      * loggin
      * @param filename - ruta del archivo que realiza la coonfiguracion del logger
      */
     getPackageFileName(filename) {
         filename = filename.replace(process.cwd(), "").replace(/\\/g, "/");
         return filename.split("/").join(".");
     }
 }
 module.exports = Log4JsService;