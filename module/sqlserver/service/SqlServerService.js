"use strict";
/**
 *@author Andres Felipe Gonzalez
 *@copyright SmartSoft 2017
 */

const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

const {Request, TYPES} = require('tedious');
const {VarChar, Int, Bit, DateTime, Date, Numeric} = TYPES;

const SqlServerException = require('../exception/SqlServerException');
const ConnectionPool = require('tedious-connection-pool');

let instance = null;

/**
 * SIngleton de la clase de conexion con sql server
 */
class SqlServerService extends global.app.class.core.Service {

    constructor() {
        super(__filename);

        if (!instance) {
            instance = this;
            this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
            this._paramType = {
                varchar: VarChar,
                int: Int,
                bit: Bit,
                dateTime: DateTime,
                date: Date,
                numeric: Numeric
            };
            this._connection = null;
        }
        return instance;
    }

    connect() {
        this.connectionConfig = {
            userName: this.config.user,
            password: this.config.password,
            server: this.config.host,
            options: {
                useUTC: this.config.useUTC,
                port: this.config.port,
                database: this.config.database,
                connectTimeout: this.config.connectTimeout,
                requestTimeout : this.config.requestTimeout,
                rowCollectionOnDone: true,
                rowCollectionOnRequestCompletion: true
            }
        };

        this.poolConfig = {
            min: this.config.pool.min,
            max: this.config.pool.max,
            log: this.config.pool.log
        };
        this.pool = new ConnectionPool(this.poolConfig, this.connectionConfig);

        /**
         * Tratamiento de errores para el pool de conexiones
         */
        this.pool.on('error', function (err) {
            log.error(err);
        });
    }

    /**
     *Metodo que consulta un procedimiento almacenado en sqlserver
     *@param procedureName Nombre del procedimiento almacenado
     *@param inputParamenter"] parametros ["s Arreglo de parametros de entrada.
     *@param outputParameters Arreglo de parametros de salida.
     */
    execProcedure(procedureName, inputParamenters = [], outputParameters = []) {
        return new Promise((resolve, reject) => {
            log.debug(`procedimiento alamcenado : ${procedureName}`);
            log.trace(`parametros : ${JSON.stringify(inputParamenters)}`);
            this.pool.acquire((err, connection) => {
                    if (err) {
                        log.error("Ocurrió un error estableciendo la conexión");
                        let {code, message} = err;
                        switch (code) {
                            case 'ETIMEOUT' : {
                                reject(new SqlServerException(2003, __filename, {error: message}));
                                break;
                            }
                            case 'ELOGIN' : {
                                reject(new SqlServerException(2004, __filename, {error: message}));
                                break;
                            }
                        }
                        reject(err);
                    } else {
                        try {
                            let request = new Request(procedureName, (error, rowCount, rows) => {
                                this._closeConnection(connection);
                                if (error) {
                                    log.error(error);
                                    reject(new SqlServerException(2001, __filename, {
                                        procedure: procedureName,
                                        error: error
                                    }));
                                } else {
                                    resolve(rows.map(row => {
                                        let rowObject = {};
                                        row.forEach(column => {
                                            rowObject[column['metadata']['colName']] = column['value'];
                                        });
                                        return rowObject;
                                    }));
                                }
                            });

                            request.on('returnValue', (name, value) => {
                                outputParameters.find(parameter => parameter.name === name).value = value;
                            });

                            request.on('doneInProc', (rowCount, more, rows) => {
                                log.debug("Finaliza la ejecucion del procedimiento almacenado");
                            });

                            inputParamenters.forEach(parameter => {
                                let {name, type, value} = parameter;
                                log.trace(name + ' => ' + JSON.stringify(type) + " : " + value);
                                request.addParameter(name, type, value);
                            });

                            outputParameters.forEach(parameter => {
                                let {name, type} = parameter;
                                request.addOutputParameter(name, type);
                            });

                            connection.callProcedure(request);
                        } catch (error) {
                            reject(error);
                        }
                    }
                }
            );

        });
    }

    /**
     *Metodo que realiza llamdas a motor de base de datos de Sql Server
     *@param statement Sentencia sql que se desea ejecutar
     *@param inputParamenter"] parametros ["s Arreglo de parametros de entrada.
     *@param outputParameters Arreglo de parametros de salida.
     */
    execStatement(statement, inputParamenters = [], outputParameters = []) {
        return new Promise((resolve, reject) => {
            try {
                log.debug("Realizando ejecución de sql");
                log.info("Obteniendo conexion :" + JSON.stringify(this.connectionConfig));
                this.pool.acquire((err, connection) => {
                    log.debug('Se obtuvo la conexión con la base de datos');
                    if (err) {
                        this._closeConnection(connection);
                        let {code, message} = err;
                        switch (code) {
                            case 'ETIMEOUT' : {
                                reject(new SqlServerException(2003, __filename, {error: message}));
                                break;
                            }
                            case 'ELOGIN' : {
                                reject(new SqlServerException(2004, __filename, {error: message}));
                                break;
                            }
                        }
                    } else {
                        let request = new Request(statement, (err, rowCount, rows) => {
                            this._closeConnection(connection);
                            //log.debug(err, rowCount, rows);
                            if (err) {
                                log.error(err);
                                reject(new SqlServerException(2001, __filename, {
                                    procedure: statement,
                                    error: err
                                }));
                            } else {
                                resolve(rows.map(row => {
                                    let rowObject = {};
                                    row.forEach(column => {
                                        rowObject[column.metadata.colName] = column.value != null ? this._extractValue(column.value, column.metadata.type.type) : null;
                                    });
                                    return rowObject;
                                }));
                            }
                        });

                        request.on('done', function (rowCount, more) {
                            log.debug('Se obtuvieron [' + rowCount + '] registros');
                        });
                        connection.execSql(request);
                    }
                });
                /*connection.on('error', function (err) {
                    log.error("Ocurrio un error estableciendo la conexion");
                    log.error(err);
                });*/
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * Funcion que realiza el proceso de extraccion y conversion del valor recibido de la
     * base de datos, este proceso de extraccion es realizado por la funcion de ejeucion directa
     * de consultas sql
     * @param value valor en formato string que se desea convertir
     * @param type tipo de variable que se optiene del la libreria tedious
     * @returns valor convertido al tipo relacionado con el atributo type
     * @private
     */
    _extractValue(value, type) {
        let option = this.config.mapping[type];
        switch (option) {
            case 'INTEGER': {
                return parseInt(value);
            }
            case 'FLOAT': {
                return parseFloat(value);
            }
            case 'DOUBLE': {
                return parseFloat(value).toFixed(2);
            }
            case 'DECIMAL': {
                return parseFloat(value).toFixed(2);
            }
            case 'STRING': {
                return value.toString();
            }
            case 'NULL': {
                return null;
            }
            case 'NONE': {
                log.error("Conversion no implementada para la extraccion de valores");
                log.error("VALUE : ", value, "TYPE", option);
                return undefined;
            }
            default : {
                log.error("Conversion no implementada para la extraccion de valores");
                log.error("VALUE : ", value, "TYPE", option);
                return undefined;
            }
        }
    }

    /**
     * Metodo que realiza el cierre de la conexion con sqlserver
     * @private
     */
    _closeConnection(connection) {
        connection.release();
        //log.trace("Cerrando conexion con SqlServer");
        //log.trace("Estado connexion", connection !== null);
        //connection.close();
        //connection = null;
    }

    get paramType() {
        return this._paramType;
    }

    set paramType(value) {
        this._paramType = value;
    }

    setConfigSqlServer(config){
        for(let key in config){
            this.config[key] = config[key]
        }
    }
}

module.exports = SqlServerService;