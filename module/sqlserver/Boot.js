"use strict";

const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));


const SqlServerService = require('./service/SqlServerService');

class Boot extends global.app.class.core.ModuleBootCore {
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
        global.app.instances.sqlServerService = new SqlServerService();
        if (this.config.validateConnection) {
            global.app.instances.sqlServerService.connect();
        }
    }

    setup() {
        log.info("Cargando modulo de SqlServerService")
    }
}

module.exports = Boot;