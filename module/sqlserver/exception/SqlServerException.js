"use strict";
/**
 *@author Andres Felipe Gonzalez
 *@copyright SmartSoft 2017
 */


class SqlServerException extends global.app.class.exception.Exception {
    /**
     * Constructor de la clase
     * @param code codigo de la excepcion
     * @param filename ruta del origen de la exepcion
     * @param object datos informativos de la exepcion
     * @param status
     */
    constructor(code,filename, object, status) {
        super(code, filename, object, status);
    }
}

module.exports = SqlServerException;


