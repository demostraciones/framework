"use strict";

// Importacion de dependencias de la clase
let AppError = require('../../../core/module/exception/bo/AppError');

/**
 * Clase para la creacion de errores de validacion de esquemas
 */
class ValidationSchemaError extends AppError {
    constructor(message,code){
        super(message || 'Error de validacion e esquema',code);
    }
}
module.exports = ValidationSchemaError;
