"use strict";
/**
 * @author Oscar Beltran
 * @copyright SmartSoft -2017
 */

/**
 * Importacion de dependencias
 */
let UtilService = require("./service/UtilService");

class UtilBoot {
    /**
     * Constructor de la clase
     */
    constructor() {
        global.utilService = new UtilService();
    }

    /**
     * Función de configuracion del modulo
     */
    setup() {
        console.log("Configurando Modulo Utils");
    }
}
module.exports = UtilBoot;
