'use strict';
/**
 * Sección de definición de las dependencias
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));
const elasticsearch = require('elasticsearch');
/**
 * Clase de manejo de conexiones con elasticsearch
 * @author Oscar Beltran - obeltran@smartsoftint.com
 * @copyright SmartSoft - 2017
 */

class ElasticSearchService extends global.app.class.core.Service {
    /**
     * Constructor de la clase
     * @param config objeto json con los parametros de configuracion de la conexion con
     * el servidor de elasticsearch
     */
    constructor(config = null) {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
    }

    validateConnection(){
        let client = this._getConnection();
        client.ping({
            requestTimeout: 30000,
        }, function (error) {
            if (error) {
                log.error('Error de conexion con elasticsearch');
            } else {
                log.info('Conexion exitosa');
            }
        });
    }
    /**
     * Función que obtiene la conexion con elasticSearch
     * @return
     */
    _getConnection() {
        return new elasticsearch.Client({
            host: `${this.config.host}:${this.config.port}`
        });
    }

    /**
     * Método para insertar o actualizar un indice
     * @param {string} type
     * @param {string} id
     * @returns {Promise}
     */
    create(type, id, data) {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            connection.index({
                index: this.config.index,
                type: type,
                id: id,
                body: data
            }, function (error, response) {
                connection.close();
                if (error) {
                    reject(error);
                }
                resolve(response);
            });
        });
    }

    /**
     * Método para obtener un documento específico
     * @param {string} index
     * @param {string} type
     * @param {string} id
     * @returns {Promise}
     */
    get(type, id) {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: this.config.index,
                type: type,
                id : id
            };
            connection.get(params, (error, response) => {
                connection.close();
                if (error) {
                    reject(error);
                    return;
                }
                resolve(response);
            });
        });
    }

    /**
     * Método para actualización de documento
     * @param {string} type
     * @param {string} id
     * @param {Object} object
     * @returns {Promise}
     */
    update(type, id, object){
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: this.config.index,
                type: type,
                id : id,
                body: {
                    doc: object
                }
            };
            connection.update(params, (error, response) => {
                connection.close();
                if (error) {
                    reject(error);
                    return;
                }
                resolve(response);
            });
        });
    }

    /**
     * Metodo que permite realizar el proceso de eliminacion en elasticsearch,
     * los parametos son opcionaes lo cual indica que es posible realizar
     * la eliminacion del indice, en el  caso de no indicar ingun valor a los
     * parametros
     * @param type - indica eñ tipo de docuemto
     * @param id - indica el indice del documento
     */
    delete(type, id) {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: this.config.index,
                type: type,
                id : id
            };
            connection.delete(params, (error, response) => {
                connection.close();
                if (error) {
                    reject(error);
                }
                else{
                    resolve(response);
                }
            });
        });
    }

    /**
     * Metodo para documentos de uno o mas indices
     */
    deleteAllIndices() {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: '_all'
            };
            connection.indices.delete(params, (error, response) => {
                connection.close();
                if (error) {
                    reject(error);
                }
                else{
                    resolve(response);
                }
            });
        });
    }

    /**
     * Metodo para realizar consultas sobre el motor de elasticSearch
     * @param {string} type
     * @param {string} query
     * @param {number} size
     * @returns {Promise}
     */
    search(type, query = null, size = 1000) {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: this.config.index,
                type : type,
                size : size
            };
            if(query !== null){
                params.q = query;
            }

            try{
                connection.search(params, (error, response) => {
                    connection.close();
                    if(error){
                        reject(error);
                        return;
                    }
                    resolve(response);
                });
            }catch(err){
                reject(error);
            }
        });
    }

    /**
     * Metodo para hacer consultas en elastic pasando objetos en el formato de las consultas del elastic query
     * @param type
     * @param query
     * @param size
     * @returns {Promise<any>}
     */
    searchWithObject(type, query , size = 1000){
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            let params = {
                index: this.config.index,
                type : type,
                size : size,
                body: query
            };
            try{
                connection.search(params, (error, response) => {
                    connection.close();
                    if(error){
                        reject(error);
                        return;
                    }
                    resolve(response);
                });
            }catch(err){
                reject(error);
            }
        });
    }

    /**
     * Método para crear un indice en elasticsearch
     * @param {string} index
     * @returns {Promise}
     */
    createIndex(index) {
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            connection.indices.create({ index }, (error, response) => {
                connection.close();
                if(error){
                   reject(error);
                   return;
                }
                resolve(response);
            });
        });
    }

    /**
     * Metodo para cargar cantidades grandes de datos a Elastic
     * @param dataBulk Array con indices a donde seran cargados y los datos
     * @return {Promise} retorna la confirmacion de carga o el error
     */
    bulk(dataBulk){
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            connection.bulk({body:dataBulk},(error, response) => {
                connection.close();
                if(error){
                    reject(error);
                    return;
                }
                resolve(response);
            });
        });
    }

    /**
     *
     * @param index
     */
    indexExists(index){
        return new Promise((resolve, reject) => {
            let connection = this._getConnection();
            connection.indices.exists({ index }, (error, response) => {
                connection.close();
                if(error){
                    reject(error);
                    return;
                }
                resolve(response);
            });
        });
    }
}

module.exports = ElasticSearchService;