"use strict";

let log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));


let ElasticService = require('./service/ElasticService');

class Boot extends global.app.class.core.ModuleBootCore {
    constructor() {
        super(__filename);
        this.config = this.ConfigurationService.getConfigurationModuleConfig(this.module);
        global.app.instances.elasticService = new ElasticService();
        if (this.config.validateConnection) {
            global.app.instances.elasticService.validateConnection();
        }
    }

    setup() {
        log.info("Cargando modulo de elasticSearch")
    }
}

module.exports = Boot;