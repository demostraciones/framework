'use strict';

/**
 *Sección de importación de dependencias
 */
const socketIo = require('socket.io');
const ChannelController = require('./bo/ChannelController');
/**
 * Definición de la variable de registro de logs
 * @type {Logger}
 */
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));

/**
 * Clase de carga de modulos de socketio y configuraciones
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSfot - 2018
 */
class Boot extends global.app.class.core.ModuleBootCore {
    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
        global.appSocketIo = socketIo(global.httpServer);
        global.app.class.modules.socketIo.ChannelController = ChannelController;
        super.setup();
    }

    setup() {
        log.info(`Iniciando configuración y carga de socketIo`);
    }

    init() {
        log.info('realizando configuracion de canal general de socketio');
        global.appSocketIo.on('connection', (socket) => {
            log.info(`usuario conectado al socket con id : ${socket.id}`);
        });
    }
}

module.exports = Boot;