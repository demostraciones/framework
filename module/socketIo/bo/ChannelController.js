"use strict";
const log = global.Logging.getLogger(global.Logging.getPackageFileName(__filename));
/**
 * clase generica para la creacion de canales con socketio
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class ChannelController extends global.app.class.core.Controller {

    /**
     * Constructor de la clase
     * @param {string} filename ruta del archivo que hereda esta clase
     */
    constructor(filename, pathChannel) {
        log.debug(`establecion canal ${pathChannel}`);
        super(filename);
        this._pathChannel = pathChannel;
        this._channel = global.appSocketIo.of(this._pathChannel);
        this.setup();
    }

    setup(){
        this._channel.on('connection', (socket)=>{
            log.info(`usuario conectado al socket con id : ${socket.id}`);
        });
    }
    /**
     * Funcion utilizada para emitir mensajes a los suscriptores del canal
     * @param data
     */
    emit(tag,data) {
        this._channel.emit(tag,data);
    }
}

module.exports = ChannelController;