const ModuleBootCore = require('./bo/ModuleBootCore');
const ModuleBoot = require('./bo/ModuleBoot');
const ModuleClusterBoot = require('./bo/ModuleClusterBoot');
const Controller = require('./bo/Controller');
const Service = require('./bo/Service');
const Repository = require('./bo/Repository');


/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class Boot extends ModuleBootCore {

    constructor() {

        super(__filename);
    }

    setup() {
        /**
         * Se realiza la carga de las clases del core del Framework
         */

        global.app.class.core.ModuleBootCore = ModuleBootCore;
        global.app.class.core.ModuleBoot = ModuleBoot;
        global.app.class.core.ModuleClusterBoot = ModuleClusterBoot;
        global.app.class.core.Controller = Controller;
        global.app.class.core.Service = Service;
        global.app.class.core.Repository = Repository;

        super.setup();
    }

    init(){

        super.init();
    }
}

module.exports = Boot;