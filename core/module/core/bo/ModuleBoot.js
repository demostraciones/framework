"use strict";

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class ModuleBoot extends global.app.class.core.CoreObject {

    /**
     * Constructor de la clase
     * @param {string} filename ruta del archivo que hereda esta clase
     */
    constructor(filename){
        super();
        this.module = this.ModuleService.getNameModule(filename);
        this.modulePath = this.ModuleService.getPathModule(filename);

    }
    setup(){}
    init(){}
}
module.exports = ModuleBoot;