let instance;
// TODO : Brayan - en desarrollo
/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class Singleton extends global.app.class.core.CoreObject{

    /**
     * Constructor de la clase
     */
    constructor() {
        super();
        if (instance) {
            instance = this;
            this.setup();
        }
    }

    setup() {
        this.init();
    }

    init() {
    }
};

module.exports = Singleton;