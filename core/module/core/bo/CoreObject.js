"use strict";

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class CoreObject {

    /**
     * Constructor de la clase
     */
    constructor() {
        this.CoreService = require('../../util/service/CoreService');
        this.ConfigurationService = require('../../util/service/ConfigurationService');
        this.ModuleService = require('../../util/service/ModuleService');
    }
}

module.exports = CoreObject;