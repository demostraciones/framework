"use strict";

const LoaderGlobalVariableService = require('../../loaderModule/service/LoaderGlobalVariableService');

class CoreService {

    static setGlobalVariable(scope, nameVariable, value, module, enviroment) {
        LoaderGlobalVariableService.setGlobalVariable(scope, nameVariable, value, module, enviroment);
    }

    static setGlobalCoreVariable(nameVariable, value) {
        LoaderGlobalVariableService.setGlobalCoreVariable(nameVariable, value);
    }

    static setGlobalApplicationVariable(module, nameVariable, value, enviroment) {
        LoaderGlobalVariableService.setGlobalApplicationVariable(module, nameVariable, value, enviroment);
    }

    static getGlobalVariable(scope, nameAtribute, value, module, enviroment) {
        return LoaderGlobalVariableService.getGlobalVariable(scope, nameAtribute, value, module, enviroment);
    }

    static getGlobalCoreVariable(nameAtribute) {
        return LoaderGlobalVariableService.getGlobalCoreVariable(nameAtribute);
    }

    static getGlobalApplicationVariable(nameVariable, module, enviroment) {
        return LoaderGlobalVariableService.getGlobalApplicationVariable(nameVariable, module, enviroment);
    }

    static printGlobalCoreVariables() {
        LoaderGlobalVariableService.printGlobalCoreVariables();
    }

    static printGlobalApplicationVariables() {
        LoaderGlobalVariableService.printGlobalApplicationVariables();
    }


}

module.exports = CoreService;