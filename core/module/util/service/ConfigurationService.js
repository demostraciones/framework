"use strict";

const LoaderConfigurationService = require('../../loaderModule/service/LoaderConfigurationService');

class ConfigurationService {
    constructor() {
    }

    static getConfigurationLibraries() {
        return LoaderConfigurationService.getLibraries();
    }

    static getModules() {
        return LoaderConfigurationService.getModules();
    }

    static getConfigurationModuleConfig(module){
        return LoaderConfigurationService.getConfigurationModuleConfig(module);
    }
    static getConfigurationModuleConstant(module){
        return LoaderConfigurationService.getConfigurationModuleConstant(module);
    }

    static getConfigurationModuleError(module){
        return LoaderConfigurationService.getConfigurationModuleError(module);
    }
}

module.exports = ConfigurationService;