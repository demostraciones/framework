"use strict";

const path = require('path');
class ModuleService {
    constructor() {
    }

    /**
     * Funcion que obtiene el nombre del modulo tomando como parametro la ruta del archivo
     * pasado por parametro
     * @param filename - ruta en string de archivo __filename
     */
    static getNameModule(filename) {
        let pathArray = filename.replace(/[\\*/*]+/g, " ").trim().split(" ");
        return pathArray[pathArray.indexOf('module') + 1];
    }

    /**
     *Funcion que obtiene la ruta del modulo tomando como parametro la ruta del archivo
     * pasado por parametro
     * @param filename - ruta en string de archivo __filename
     */
    static getPathModule(filename) {
        let pathArray = filename.replace(/[\\*/*]+/g, " ").trim().split(" ");
        return pathArray.slice(0,pathArray.indexOf('module') + 2).join(path.sep);
    }
}
module.exports = ModuleService;
