"use strict";

/**
 * clase que modela la estructura de manaejo de errores de la aplicación
 * @see https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */


class Exception extends Error {

    constructor(code, pathFilename, object=undefined, status=500) {
        let log;
        if (global.Logging){
            log = global.Logging.getLogger(global.Logging.getPackageFileName(pathFilename));
        }else{
            log = console;
        }
        let moduleArray = pathFilename
            .replace(/[\\*/*]+/g, " ")
            .trim()
            .split(" ");
        let indexModule = moduleArray.indexOf('module');
        let module = pathFilename;

        let message = 'Ocurrio un error en la ruta ' + pathFilename;

        if (indexModule !== -1 ){
            module = moduleArray[indexModule+1];
            if (typeof object === 'object') {
                message = Exception.interpolate(global.app.exception[module][code + ''], object);
            } else if (typeof object === 'string') {
                message = object;
            } else if (!object){
                log.error(global.app);
                message = global.app.exception[module][code + ''];
            }
        }
        super(message);
        Error.captureStackTrace(this, this.constructor);
        this._timestamp = Date.now();
        this._status = status;
        this._code = code;
        this._pathFilename = pathFilename;
        this._name = this.constructor.name;
        this._module = module;
        this._message = message;
        this._object = object;
        log.error(this.message);
    }

    get code() {
        return this._code;
    }

    set code(value) {
        this._code = value;
    }

    get pathFilename() {
        return this._pathFilename;
    }

    set pathFilename(value) {
        this._pathFilename = value;
    }

    get object() {
        return this._object;
    }

    set object(value) {
        this._object = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get timestamp() {
        return this._timestamp;
    }

    set timestamp(value) {
        this._timestamp = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get module() {
        return this._module;
    }

    set module(value) {
        this._module = value;
    }

    get message() {
        return this._message;
    }

    set message(value) {
        this._message = value;
    }

    /**
     * @param object
     * @private
     */
    static interpolate(message, object) {
        return message.replace(/{([^{}]*)}/g, (a, b) => {
                let r = object[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    }

    /**
     * Funcion que realiza la generación de la
     * respuesta de error para los servicios rest
     * de la aplicación
     * @returns {AppError}
     */
    toRestResponse() {
        return this;
    }
}

module.exports = Exception;