"use strict";

/**
 * https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
 * clase que modela la estructura de manaejo de errores de la aplicación
 * para el caso de recolectar multiples errores
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class ExceptionList extends Error {
    /**
     * Constructor de la clase
     * @param message
     * @param code
     * @param status
     */
    constructor(exceptionsList) {
        super("");
        // Capturing stack trace, excluding constructor call from it.
        Error.captureStackTrace(this, this.constructor);
        this.timestamp = Date.now();
        // Guardando el nombre de la clase de error
        this.name = this.constructor.name;
        // Guardando el codigo de error general, el valor por defecto es 500
        this.status = 500;
        //Obteniendo la ubicacion del archivo que genera el error        
        this._exceptionsList = exceptionsList;
    }

    get exceptionsList() {
        return this._exceptionsList;
    }

    /**
     * Funcion que realiza la generación de la
     * respuesta de error para los servicios rest
     * de la aplicación
     * @returns {AppError}
     */
    toRestResponse() {
        return this;
    }
}

module.exports = ExceptionList;