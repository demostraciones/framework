"use strict";

const log = Logging.getLogger(Logging.getPackageFileName(__filename));

/**
 * @author Brayan Hamer Rodriguez Sanchez
 * @copyright SmartSoft - 2017
 */
class Boot extends global.app.class.core.ModuleBootCore{

    /**
     * Constructor de la clase
     */
    constructor() {
        super(__filename);
    }

    /**
     * Función que realiza la carga dinamica de la
     * definción de las clases principales de manejo de exepciones
     * en la aplicación
     */
    setup() {
        log.info("Realizando registro de excepciones del core del framework");
        global.app.class.exception.Exception = require('./bo/Exception');
        global.app.class.exception.ExceptionList = require('./bo/ExceptionList');
        log.debug("[AppError, AppListError]");
        super.setup();
    }
}
module.exports = Boot;
