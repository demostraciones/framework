"use strict";

const log = Logging.getLogger(Logging.getPackageFileName(__filename));
/**
 *Sección de importación de dependencias
 */
const LoaderConfigurationService = require('./service/LoaderConfigurationService');
const LoaderGlobalVariableService = require('./service/LoaderGlobalVariableService');
const LoaderModuleService = require('./service/LoaderModuleService');
const LoaderModuleLibraryService = require('./service/LoaderGloablLibraryService');

/**
 * Clase que realiza la carga de las librerias necesarias para el proyecto inyectandolas como variables para su uso en
 * el contexto del proyecto, adicionalmente realiza la carga inicial los modulos del proyecto
 *
 * @author Brayan Hamer Rodriguez sanchez - bhamerrsanchez@gmail.com
 * @copyright Smartsoft - 2017
 */
class Boot extends global.app.class.core.ModuleBootCore {
    /**
     * Constructor de la clase
     */
    constructor(cwd = process.cwd()) {
        super(__filename);
        // se realiza la craga de las variables globales de la aplciacion
        new LoaderGlobalVariableService(cwd);
        // se realiza la asignacion de los metodos estaticos de la clase a la variable global
        this._loaderConfigurationService = new LoaderConfigurationService();
        this._loaderConfigurationService.setup();
        this._loaderModuleLibraryService = new LoaderModuleLibraryService();
        this._loaderModuleService = new LoaderModuleService();
    }

    /**
     * Función que realiza el flujo de trabajo de ejecucion y de definicion
     * de todas las dependencias principales y preconfiguradas del proyecto
     */
    setup() {
        try {
            this._loaderModuleLibraryService.setup();
            this._loaderModuleService.loadModulesCore();
            this._loaderModuleService.loadModulesProject();
        } catch (error) {
            console.error(error);
        }
        super.setup();
    }

}

module.exports = Boot;