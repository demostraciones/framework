"use strict";

const log = Logging.getLogger(Logging.getPackageFileName(__filename));

/**
 * Sección de importación de las dependencias
 */
const Loader = require('../bo/Loader');
const fs = require('fs');
const path = require('path');

/**
 * Clase que realiza la gestión y carga de modulos definidos en la aplicación,
 * dentro de la logica del servicio se realiza la carga de los modulos
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */
class LoaderModuleService {

    /**
     * Constructo de la clase
     */
    constructor() {
        this._loaders = {};
        this._loadersProject = {};
        this._orderLoad = new Set();
        /*
        this._orderLoad = new Set(global.app.configuration.core.orderExecution.map(value => {
            return value.toLocaleString();
        }));
        */
    }

    /**
     * Función que realiza el proceso de carga de todos los modulos
     * de configuracion del proyecto y los modulos creados
     * en el proyecto
     */
    setup() {
        log.debug("Cargando modulos");
        log.debug("Cargando modulos de la siguiente ruta : ");
        this.loadModulesCore();
    }

    /**
     * Función que realiza la carga de todos los modulos de la carpeta config
     * excluyendo el modulo loaderModule, ya que este modulo se llama al inicar la aplicación
     * este metodo depende de la definicion de la variable global PATH_CONFIG_MODULES,
     * esta variable en configurada en el Boot.js del modulo loaderModule
     */
    loadModulesCore() {
        let pathProjectFrameworkModules = global.app.util.CoreService.getGlobalCoreVariable('PATH_PROJECT_FRAMEWORK_MODULES');
        log.debug("Lectura de modulos extra del core de la ruta");
        fs.readdirSync(pathProjectFrameworkModules).forEach(file => {
            if (global.app.configuration.core.modules[file]) {
                if (fs.existsSync(path.resolve(pathProjectFrameworkModules,file,'resources','error.json'))) {
                    global.app.exception[file] = require(path.resolve(pathProjectFrameworkModules,file,'resources','error'));
                }
                if (fs.existsSync(path.resolve(pathProjectFrameworkModules,file,'resources','constans.json'))) {
                    global.app.configuration.core.modules[file].constans = require(path.resolve(pathProjectFrameworkModules,file,'resources','constants'));
                }
                this._loaders[file] = new Loader(file, pathProjectFrameworkModules);
                this._orderLoad.add(file);
            }
        });
        log.trace(this._orderLoad);
        log.info(`Iniciando la carga de los modulos del core configurados `);
        log.info(`MODULOS DEL CORE :`);
        this._orderLoad.forEach((nameModule) => {
            log.debug(nameModule, this._loaders[nameModule].nameBootFile);
            let Module = require(this._loaders[nameModule].nameBootFile);
            let moduleInstance = new Module();
            moduleInstance.setup();
        });
    }

    /**
     * Funcion que realiza la carga de todos los modulos del proyecto ubicados en la carpeta src/module/
     * estos modulos depeden de que la defincion de archivo boot contenga una clase con la funcion setup,
     * ya que es la funcion llamada en el proceso de configuracion de la palicación, en tiempo de ejecución
     */
    loadModulesProject() {
        log.debug("Lectura de modulos del proyecto y archivos de configuración principales");
        let pathProjectModules = global.app.util.CoreService.getGlobalCoreVariable('PATH_PROJECT_MODULES');
        fs.readdirSync(pathProjectModules).forEach(file => {
            if (global.app.configuration.modules[file]) {
                if (fs.existsSync(path.resolve(pathProjectModules,file,'resources','error.json'))) {
                    global.app.exception[file] = require(path.resolve(pathProjectModules,file,'resources','error'));
                }
                if (fs.existsSync(path.resolve(pathProjectModules,file,'resources','constants.json'))) {
                    global.app.modules[file].constans = require(path.resolve(pathProjectModules,file,'resources','constants'));
                }
                this._loadersProject[file] = new Loader(file, pathProjectModules);
            }
        });
        log.info('Realizando carga de los modulos del proyecto');
        for (let nameModule in this._loadersProject) {
            log.info(nameModule, this._loadersProject[nameModule].nameBootFile);
            let Module = require(this._loadersProject[nameModule].nameBootFile);
            let moduleInstance = new Module();
            moduleInstance.setup();
        }
    }
}

module.exports = LoaderModuleService;


