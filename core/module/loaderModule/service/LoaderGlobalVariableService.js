const log = global.Logging.getLogger(Logging.getPackageFileName(__filename));
const path = require('path');

let SCOPE = {
    CORE: "core",
    APP: "app"
};

class LoaderGlobalVariableService {


    constructor(cwd = process.cwd()) {
        this.SCOPE = SCOPE;

        this.setupDefaultVariables(cwd);
    }

    setupDefaultVariables(cwd = process.cwd()) {
        log.debug(`Configurando variables globales del Core`);
        LoaderGlobalVariableService.setGlobalCoreVariable('ENVIROMENT', process.env.NODE_ENV !== undefined && process.env.NODE_ENV !== '' ? process.env.NODE_ENV : '');
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_ROOT', cwd + path.sep);
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_ROOT_RESOURCE', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT_ROOT'), 'resources'));
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_FRAMEWORK', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT_ROOT'), 'framework'));
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_FRAMEWORK_RESOURCE', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT_FRAMEWORK'), 'resources'));
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_FRAMEWORK_MODULES', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT_FRAMEWORK'), 'module'));

        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT_ROOT'), 'src'));

        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_RESOURCE', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT'), 'resources'));
        LoaderGlobalVariableService.setGlobalCoreVariable('PATH_PROJECT_MODULES', path.resolve(LoaderGlobalVariableService.getGlobalCoreVariable('PATH_PROJECT'), 'module'));

        LoaderGlobalVariableService.printGlobalCoreVariables();
    }

    static setGlobalVariable(scope, nameVariable, value, module, enviroment = '') {
        if (scope === LoaderGlobalVariableService.SCOPE.CORE) {
            LoaderGlobalVariableService.setGlobalCoreVariable(nameVariable, value);
        } else if (scope === LoaderGlobalVariableService.SCOPE.APP) {
            LoaderGlobalVariableService.setGlobalApplicationVariable(module, nameVariable, value, enviroment);
        }
    }

    static setGlobalCoreVariable(nameVariable, value) {
        global.app.variables.core[nameVariable] = value;
    }

    static setGlobalApplicationVariable(module, nameVariable, value, enviroment = '') {

        if (!global.app.variables.app[module]) {
            global.app.variables.app[module] = {}
        }
        enviroment = LoaderGlobalVariableService.getGlobalCoreVariable('ENVIROMENT');
        if (!enviroment) {

        } else if (enviroment !== '') {
            if (!global.app.variables.app[module][enviroment]) {
                global.app.variables.app[module][enviroment] = {}
            }
            global.app.variables.app[module][enviroment][nameVariable] = value;

        } else {
            if (!global.app.variables.app[module]) {
                global.app.variables.app[module] = {}
            }
            global.app.variables.app[module][nameVariable] = value;
        }
    }

    static getGlobalVariable(scope, nameAtribute, value, module, enviroment = '') {
        if (scope === SCOPE.CORE) {
            LoaderGlobalVariableService.getGlobalCoreVariable(nameAtribute, value);
        } else if (scope === SCOPE.APP) {
            LoaderGlobalVariableService.getGlobalApplicationVariable(module, nameAtribute, value, enviroment);
        }
    }

    static getGlobalCoreVariable(nameAtribute) {
        return global.app.variables.core[nameAtribute];
    }

    static getGlobalApplicationVariable(nameVariable, module, enviroment = '') {
        if (!module) {
            console.log(`es obligatorio el parametro module`);
            return undefined;
        } else {
            if (enviroment !== '') {
                if (!global.app.variables.app[module][enviroment][nameVariable]) {
                    console.warn(`Variable no encontrada para los parametros modulo : [${module}], ambiente [${enviroment}], variable : [${nameVariable}]`)
                }
                return global.app.variables.app[module][enviroment][nameVariable];
            } else {
                if (!global.app.variables.app[module][nameVariable]) {
                    console.warn(`Variable no encontrada para los parametros modulo : [${module}], ambiente [${enviroment}], variable : [${nameVariable}]`)
                }
                return global.app.variables.app[module][nameVariable];
            }
        }
    }

    static printGlobalCoreVariables() {
        for (let key in global.app.variables.core) {
            let value = global.app.variables.core[key];
            log.debug(`${key} : ${JSON.stringify(value)}`);
        }
    }

    static printGlobalApplicationVariables() {
        for (let key in global.app.variables.app) {
            let value = global.app.variables.app[key];
            log.debug(`${key} : ${JSON.stringify(value)}`);
        }
    }
}

module.exports = LoaderGlobalVariableService;