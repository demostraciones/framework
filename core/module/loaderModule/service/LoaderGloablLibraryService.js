"use strict";

const log = Logging.getLogger(Logging.getPackageFileName(__filename));

/**
 * Clase que realiza la carga de las dependencias globlaes
 * de la aplicación
 * @author Brayan Hamer Rodriguez Sanchez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */

class LoaderModuleLibraryService {
    /**
     * Constructor de la clase
     * @param libraries - objeto con las libreria a cargar
     * la estructura para este objeto es {"nombre de la variable":"libreria"}
     * por ejemplo: la libreria de lodash se carga en la variable global "_"
     * la deifnición del objeto seria {"_":"lodash"}
     */
    constructor(){
        this._libraries = require('./LoaderConfigurationService').getLibraries();
    }

    /**
     * Función que realiza la carga dinamica de librerias definidas
     * en el archivo de configuracion del modulo
     * @return void
     */
    setup(){
        log.debug("Iniciando Registro De librerias globales");
        for (var key in this._libraries){
            log.debug('\t['+key+']:['+this._libraries[key]+']');
            try{
            global[key] = require(this._libraries[key]);
            }catch (error){
                throw (error);
            }
        }
        log.debug("Registro de librerias globales finalizado");
    }
}
module.exports = LoaderModuleLibraryService;
