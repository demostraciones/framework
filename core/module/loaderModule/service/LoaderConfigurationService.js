"use strict";
const log = Logging.getLogger(Logging.getPackageFileName(__filename));
/**
 * Seccion de carga de dependencias
 */
const path = require('path');
const CONFIG_DEFAULT_FILES = {
    APPLICATION: "application.json",
    CONFIG: "config.json",
    ERROR: "error.json",
};

const LOAD_MODES = {
    INHERIT: "inherit",
    MERGE: "merge",
    REPLACE: "replace",
};

const fs = require('fs');

class LoaderConfigurationService {
    constructor() {
        this.core = {};
        this.app = {};
    }

    setup() {
        log.info("Realizando carga de las configuraciones del core del framework y los modulos del proyecto");
        this.readConfigurationResourceCore();
        this.readConfigurationResourceApp();
        this.buildConfiguration();
    }

    readConfigurationResourceCore() {
        this.core = require(path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_FRAMEWORK_RESOURCE"),CONFIG_DEFAULT_FILES.APPLICATION));
    }

    readConfigurationResourceApp() {
        if (fs.existsSync(path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_ROOT_RESOURCE"),CONFIG_DEFAULT_FILES.APPLICATION))) {
            this.app = require(path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_ROOT_RESOURCE"),CONFIG_DEFAULT_FILES.APPLICATION));
        } else {
            this.app = require(path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_RESOURCE"),CONFIG_DEFAULT_FILES.APPLICATION));
        }

    }

    buildConfiguration() {

        global.app.configuration.core.orderExecution = this.core.orderExecution;

        if (this.app.core.libraries) {
            global.app.configuration.core.libraries = this._mergeConfiguration(this.app.core.libraries, this.core.libraries)
        } else {
            global.app.configuration.core.libraries = this.core.libraries;
        }
        if (this.app.core) {
            this._buildCoreConfiguration(this.app.core.modules);
        } else {
            this._buildCoreConfiguration(this.core.modules);
        }
        this._buildAppConfiguration(this.app.modules);
    }

    _buildCoreConfiguration(coreModules) {
        for (let key in coreModules) {
            let pathFrameworkModules = path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_FRAMEWORK_MODULES"),key);
            global.app.configuration.core.modules[key] = {};
            if (fs.existsSync(pathFrameworkModules)) {
                switch (coreModules[key].load) {
                    case LOAD_MODES.INHERIT: {
                        global.app.configuration.core.modules[key].config = this._inheritConfiguration(path.resolve(pathFrameworkModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        break;
                    }
                    case LOAD_MODES.MERGE: {
                        let configMerge = require(path.resolve(pathFrameworkModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        configMerge = this._mergeConfiguration(coreModules[key], configMerge);
                        global.app.configuration.core.modules[key].config = configMerge;
                        break;
                    }
                    case LOAD_MODES.REPLACE : {
                        let configReplace = this._replaceConfiguration(coreModules[key]);
                        global.app.configuration.core.modules[key].config = configReplace;
                        break;
                    }
                    default : {
                        let configMerge = require(path.resolve(pathFrameworkModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        configMerge = this._mergeConfiguration(coreModules[key], configMerge);
                        global.app.configuration.core.modules[key].config = configMerge;
                    }
                }
            }
        }
    }

    _buildAppConfiguration(modules) {
        for (let key in modules) {
            let pathProjectModules = path.resolve(global.app.util.CoreService.getGlobalCoreVariable("PATH_PROJECT_MODULES"),key);
            global.app.configuration.modules[key] = {};
            if (fs.existsSync(pathProjectModules)) {
                switch (modules[key].load) {
                    case LOAD_MODES.INHERIT: {
                        global.app.configuration.modules[key].config = this._inheritConfiguration(path.resolve(pathProjectModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        break;
                    }
                    case LOAD_MODES.MERGE: {
                        let configMerge = require(path.resolve(pathProjectModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        configMerge = this._mergeConfiguration(modules[key], configMerge);
                        global.app.configuration.modules[key].config = configMerge;
                        break;
                    }
                    case LOAD_MODES.REPLACE : {
                        let configReplace = this._replaceConfiguration(modules[key]);
                        global.app.configuration.modules[key].config = configReplace;
                        break;
                    }
                    default : {
                        let configMerge = require(path.resolve(pathProjectModules,'resources',CONFIG_DEFAULT_FILES.APPLICATION));
                        configMerge = this._mergeConfiguration(modules[key], configMerge);
                        global.app.configuration.modules[key].config = configMerge;
                    }
                }
            }
        }
    }

    _mergeConfiguration(fromConfiguration, toConfiguration) {
        let resultConfiguration = toConfiguration;
        for (let porpertyKey  in fromConfiguration) {
            resultConfiguration[porpertyKey] = fromConfiguration[porpertyKey];
        }
        if (resultConfiguration['load']) {
            delete resultConfiguration['load'];
        }
        return resultConfiguration;
    }

    _inheritConfiguration(path) {
        let resultConfiguration = require(`${path}`);
        delete resultConfiguration['load'];
        return resultConfiguration;
    }

    _replaceConfiguration(toConfiguration) {
        let resultConfiguration = toConfiguration;
        delete resultConfiguration['load'];
        return resultConfiguration;
    }

    static getLibraries() {
        return global.app.configuration.core.libraries;
    }

    static getCoreModules() {
        return global.app.configuration.core.modules;
    }

    static getModules() {
        return global.app.configuration.modules;
    }

    static getConfigurationModuleConfig(module){
        if (global.app.configuration.modules[module]){
            return global.app.configuration.modules[module].config;
        }else {
            return global.app.configuration.core.modules[module].config;
        }
    }
    static getConfigurationModuleConstant(module){
        return global.app.configuration.modules[module].constant;
    }

    static getConfigurationModuleError(module){
        return global.app.configuration.modules[module].error;
    }

}

module.exports = LoaderConfigurationService;