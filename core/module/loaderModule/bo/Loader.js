"use strict";

const path = require('path');
/**
 * Clase que contienen la estructura de defincion del cargador de modulos
 * donde se indica  el nombre del modulo y la ruta de obtencion del modulo
 * @author Brayan Hamer Rodriguez Sachez - bhamerrsanchez@gmail.com
 * @copyright SmartSoft - 2017
 */
class Loader {

    /**
     * Constructor de la clase
     * @param nameModule Nombre del modulo
     * @param pathModule ruta de la ubicación del modulo
     */
    constructor(nameModule, pathModule) {
        this._nameModule =nameModule;
        this._path = path.resolve(pathModule,nameModule);
        this._nameBootFile = path.resolve(this._path,'Boot');
    }

    /*
    * Sección de definición de gets y sets de las variables
    * */

    get nameModule() {
        return this._nameModule;
    }

    set nameModule(value) {
        this._nameModule = value;
    }

    get path() {
        return this._path;
    }

    set path(value) {
        this._path = value;
    }

    get nameBootFile() {
        return this._nameBootFile;
    }

    set nameBootFile(value) {
        this._nameBootFile = value;
    }
}
module.exports = Loader;