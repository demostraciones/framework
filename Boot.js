"use strict";

const path = require('path');
let log;

class Boot {
    constructor() {
        /**
         * Definicion de objeto base del framework
         * @type {{variables: {core: {}, app: {}}, core: {}, util: {}, object: {core: {}, app: {}}, configuration: {core: {modules: {}, libraries: {}}, modules: {}}}}
         */
        global.app = {
            instances: {
                core: {},
                modules: {}
            },
            class: {
                core: {},
                modules: {},
                exception: {}
            },
            variables: {
                core: {},
                app: {}
            },
            core: {},
            util: {},
            object: {
                core: {},
                app: {}
            },
            configuration: {
                core: {
                    modules: {},
                    libraries: {}
                },
                modules: {}
            },
            exception: {}
        };
        global.app.class.core.CoreObject = require('./core/module/core/bo/CoreObject');
    }

    setup() {

        /**
         * Se realiza la carga inical del modulo de logging del core con las configuraciones por default
         * Libreria definida por default log4js
         */
        const Log4Js = require('./module/log4Js/Boot');
        let log4Js = new Log4Js();
        log4Js.setup();

        log = Logging.getLogger(Logging.getPackageFileName(__filename));
        log.info(`Iniciando aplicacion`);

        const CoreBoot = require('./core/module/core/Boot');
        new CoreBoot().setup();

        global.app.util.CoreService = require('./core/module/util/service/CoreService');

        const ExceptionBoot = require('./core/module/exception/Boot');
        new ExceptionBoot().setup();

        const LoaderModuleBoot = require('./core/module/loaderModule/Boot');
        new LoaderModuleBoot(path.resolve(__dirname,'..')).setup();
        log.trace(`
            ${JSON.stringify(global.app)}
        `);
        log4Js.reload(global.app.configuration.core.modules.log4js.config);
    }
}

module.exports = Boot;